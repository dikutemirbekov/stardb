import React from "react";
import ItemList from "../item-list";
import PersonDetails from "../person-details";
import Row from "../row";

class PeoplePage extends React.Component {
        state={
            selectedItem: 1
        }

        onselectItem =(id) => {
            this.setState({
                selectedItem: id
            })
        }
        

    render() {
        const left =<ItemList onselectItem={this.onselectItem}/>
        const rigt = <PersonDetails selectedItem={this.state.selectedItem}/>

        return (
            <Row
             leftElement={left}
             rightElement={rigt} 
            />
        )   
    }
}

export default PeoplePage;